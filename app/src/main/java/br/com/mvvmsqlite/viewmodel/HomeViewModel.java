package br.com.mvvmsqlite.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import br.com.mvvmsqlite.BR;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.view.HomeActivity;

public class HomeViewModel extends BaseObservable {

    private User user;
    private HomeActivity homeActivity;

    public HomeViewModel(User user, HomeActivity homeActivity) {
        this.user = user;
        this.homeActivity = homeActivity;
    }

    @Bindable
    public String getEmail() {
        return user.getEmail();
    }

    public void setUser(User user) {
        this.user = user;
        notifyPropertyChanged(BR.email);
    }

    public void goUpdateUser(){
        homeActivity.goUpdateUser(user);
    }
}
