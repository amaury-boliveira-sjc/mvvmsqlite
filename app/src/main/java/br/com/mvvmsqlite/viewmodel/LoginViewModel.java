package br.com.mvvmsqlite.viewmodel;

import android.content.Context;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import br.com.mvvmsqlite.BR;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.repository.UserRepository;

public class LoginViewModel extends BaseObservable {

    private User user;
    private UserRepository userRepository;

    @Bindable
    private User loggedUser;

    public LoginViewModel(Context context) {
        this.user = new User();
        this.userRepository = new UserRepository(context);
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
        notifyPropertyChanged(BR.loggedUser);
    }

    @Bindable
    public String getEmail() {
        return user.getEmail();
    }

    public void setEmail(String email) {
        user.setEmail(email);
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword(){
        return user.getPassword();
    }

    public void setPassword(String password){
        user.setPassword(password);
        notifyPropertyChanged(BR.password);
    }

    public void doLogin(){
        User loggedUser = userRepository.findByEmailAndPassword(getEmail(), getPassword());
        setLoggedUser(loggedUser);
    }
}
