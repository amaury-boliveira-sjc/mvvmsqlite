package br.com.mvvmsqlite.viewmodel;

import android.content.Context;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import br.com.mvvmsqlite.BR;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.repository.UserRepository;

public class UpdateUserViewModel extends BaseObservable {

    private User user;
    private UserRepository userRepository;

    @Bindable
    private User updatedUser;

    public UpdateUserViewModel(Context context, User user) {
        this.user = user;
        this.userRepository = new UserRepository(context);
    }

    public User getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(User updatedUser) {
        this.updatedUser = updatedUser;
        notifyPropertyChanged(BR.updatedUser);
    }

    @Bindable
    public String getEmail(){
        return user.getEmail();
    }

    public void setEmail(String email){
        user.setEmail(email);
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword(){
        return user.getPassword();
    }

    public void setPassword(String password){
        user.setPassword(password);
        notifyPropertyChanged(BR.password);
    }

    public void updateUser(){
        User updatedUser = userRepository.updateUser(user.getId(), getEmail(), getPassword());
        setUpdatedUser(updatedUser);
    }
}
