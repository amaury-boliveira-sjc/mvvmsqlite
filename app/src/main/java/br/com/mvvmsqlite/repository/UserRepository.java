package br.com.mvvmsqlite.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.com.mvvmsqlite.db.DatabaseHelper;
import br.com.mvvmsqlite.model.User;

public class UserRepository {

    private DatabaseHelper databaseHelper;

    public UserRepository(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }

    public User findByEmailAndPassword(String email, String password){
        //TODO duas formas de fazer um select retornando um cursor
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.query("user", new String[]{"id", "email", "password"}, "email = ? AND password = ?", new String[]{email, password}, null, null, null);
        /*Cursor cursor = db.rawQuery("SELECT * FROM user WHERE email = ? AND password = ?", new String[]{email, password});*/
        try {
            if (cursor.moveToFirst()){
                User user = new User();
                user.setId(cursor.getInt(0));
                user.setEmail(cursor.getString(1));
                user.setPassword(cursor.getString(2));
                return user;
            }
            return new User();
        }finally {
            cursor.close();
        }
    }

    public User updateUser(long id, String email, String password){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("email", email);
        contentValues.put("password", password);
        int rowsAffected = db.update("user", contentValues, "id = ?", new String[]{String.valueOf(id)});
        if (rowsAffected > 0){
            return findByEmailAndPassword(email, password);
        }
        return new User();
    }
}