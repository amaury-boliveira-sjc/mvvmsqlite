package br.com.mvvmsqlite.router;

import android.app.Activity;
import android.content.Intent;

import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.view.HomeActivity;

public class HomeRouter {

    private Activity activity;
    public static final int REQUEST_CODE = 0;
    public static final int RESULT_CODE = 1;

    public HomeRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.putExtra("user", user);
        activity.startActivity(intent);
        activity.finish();
    }
}
