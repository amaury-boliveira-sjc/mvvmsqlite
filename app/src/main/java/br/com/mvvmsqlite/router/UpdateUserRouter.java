package br.com.mvvmsqlite.router;

import android.app.Activity;
import android.content.Intent;

import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.view.UpdateUserActivity;

public class UpdateUserRouter {

    private Activity activity;

    public UpdateUserRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, UpdateUserActivity.class);
        intent.putExtra("user", user);
        activity.startActivityForResult(intent, HomeRouter.REQUEST_CODE);
    }
}
