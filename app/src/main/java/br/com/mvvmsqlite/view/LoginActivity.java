package br.com.mvvmsqlite.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;

import br.com.mvvmsqlite.R;
import br.com.mvvmsqlite.databinding.ActivityLoginBinding;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.router.HomeRouter;
import br.com.mvvmsqlite.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setDefaultComponent(new DataBindingComponent(){
            @Override
            public LoginActivity getLoginActivity(){
                return LoginActivity.this;
            }
            @Override
            public UpdateUserActivity getUpdateUserActivity() {
                return null;
            }
        });
        ActivityLoginBinding loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginBinding.setViewModel(new LoginViewModel(this));
        loginBinding.executePendingBindings();
    }

    @BindingAdapter({"doLogin"})
    public void doLogin(View view, User loggedUser){
        if (loggedUser != null){
            if (loggedUser.getId() != 0){
                HomeRouter homeRouter = new HomeRouter(this);
                homeRouter.go(loggedUser);
            }else{
                Toast.makeText(view.getContext(), "Erro ao realizar login", Toast.LENGTH_LONG).show();
            }
        }
    }
}
