package br.com.mvvmsqlite.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import br.com.mvvmsqlite.R;
import br.com.mvvmsqlite.databinding.ActivityHomeBinding;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.router.UpdateUserRouter;
import br.com.mvvmsqlite.router.HomeRouter;
import br.com.mvvmsqlite.viewmodel.HomeViewModel;

public class HomeActivity extends AppCompatActivity {

    private UpdateUserRouter updateUserRouter;
    private ActivityHomeBinding homeBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User user = (User) getIntent().getSerializableExtra("user");
        this.updateUserRouter = new UpdateUserRouter(this);
        this.homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        homeBinding.setViewModel(new HomeViewModel(user, this));
        homeBinding.executePendingBindings();
    }

    public void goUpdateUser(User user){
        updateUserRouter.go(user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HomeRouter.REQUEST_CODE && resultCode == HomeRouter.RESULT_CODE && data != null){
            boolean edited = data.getBooleanExtra("edited", false);
            User updatedUser = (User) data.getSerializableExtra("updatedUser");
            if (edited){
                homeBinding.getViewModel().setUser(updatedUser);
            }
        }
    }
}
