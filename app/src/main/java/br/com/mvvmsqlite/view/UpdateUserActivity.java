package br.com.mvvmsqlite.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;

import br.com.mvvmsqlite.R;
import br.com.mvvmsqlite.databinding.ActivityUserBinding;
import br.com.mvvmsqlite.model.User;
import br.com.mvvmsqlite.router.HomeRouter;
import br.com.mvvmsqlite.viewmodel.UpdateUserViewModel;

public class UpdateUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setDefaultComponent(new DataBindingComponent(){
            @Override
            public UpdateUserActivity getUpdateUserActivity() {
                return UpdateUserActivity.this;
            }
            @Override
            public LoginActivity getLoginActivity() {
                return null;
            }
        });
        User user = (User) getIntent().getSerializableExtra("user");
        ActivityUserBinding userBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        userBinding.setViewModel(new UpdateUserViewModel(this, user));
        userBinding.executePendingBindings();
    }

    @BindingAdapter({"updateUser"})
    public void updateUser(View view, User updatedUser){
        if (updatedUser != null){
            if (updatedUser.getId() != 0){
                Intent intent = new Intent();
                intent.putExtra("edited", true);
                intent.putExtra("updatedUser", updatedUser);
                setResult(HomeRouter.RESULT_CODE, intent);
                finish();
            }else{
                Toast.makeText(this, "Erro ao atualizar dados do usuário", Toast.LENGTH_LONG).show();
            }
        }
    }
}
